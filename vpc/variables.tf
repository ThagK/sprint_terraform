variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "vpc_cidr" {
  type        = string
  default     = "172.23.0.0/16"
  description = "CIDR allocated to the VPC"
}

variable "az" {
  type        = list(string)
  default     = ["eu-west-1a", "eu-west-1b"]
  description = "List of AZs"
}

variable "subnet_cidr" {
  type        = list(string)
  default     = ["172.23.1.0/24", "172.23.2.0/24"]
  description = "List of subnet CIDRs"
}


#variable "public_subnet_count" {
#  description = "Number of public subnets."
#  type        = number
#  default     = 2
#}
