#VPC CREATION. Variable "vpc_cdidr" declared inside variables.tf

resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "MainVPC"
  }
}



#SUBNET CREATION

resource "aws_subnet" "main" {
  count             = 2 #Create 2 similar subnets
  vpc_id            = aws_vpc.main.id
  availability_zone = var.az[count.index]
  cidr_block        = var.subnet_cidr[count.index]

  tags = {
    Name = "MainSUBNET ${count.index}"
  }
}


#INTERNET GATEWAY CREATION

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "MainIGATEWAY"
  }
}


#ROUTE TABLE CREATION
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
  tags = {
    Name = "MainROUTETABLE"
  }
}


#ROUTE TABLE AND SUBNET ASSOCIATION
resource "aws_route_table_association" "a" {
  count          = length(var.subnet_cidr)
  subnet_id      = element(aws_subnet.main.*.id, count.index)
  route_table_id = aws_route_table.main.id
}
