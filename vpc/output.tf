#VPC outputs

output "main_vpc_id" {
  value = aws_vpc.main.id
}

output "main_vpc_cidr" {
  value = aws_vpc.main.cidr_block
}

output "main_vpc_instance_tenancy" {
  value = aws_vpc.main.instance_tenancy
}

output "main_vpc_tags" {
  value = aws_vpc.main.tags
}


#SUBNET outputs

output "main_subnet_all" {
  value = aws_subnet.main
}

#output "main_subnet_arn" {
#  value = aws_subnet.main.arn
#}

#output "main_subnet_cidr" {
#  value = aws_subnet.main.cidr_block
#}

#output "main_subnet_alltags" {
#  value = aws_subnet.main.tags
#}


#IGW outputs

output "main_igw_id" {
  value = aws_internet_gateway.main.id
}

output "main_igw_arn" {
  value = aws_internet_gateway.main.arn
}

#ROUTETABLES outputs

output "main_rt_id" {
  value = aws_route_table.main.id
}

